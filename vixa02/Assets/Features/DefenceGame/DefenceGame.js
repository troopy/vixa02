﻿#pragma strict
import System.Collections.Generic;
import UnityEngine.EventSystems;
import UnityEngine.UI;
import UnityEngine.SceneManagement;

class DefenceGame extends MonoBehaviour
{
	public var passwordInputField : InputField;
	public var brickwalls : List.<GameObject>;
	public var enemies : List.<GameObject>;
	public var SpawnPoints : GameObject;
	public var EndPoint : GameObject;
	public var BrickPoint : GameObject;
	public var WonPanel : GameObject;
	public var ResetPanel : GameObject;
	public var livesText : Text;
	public var speed : float = 20;
	public var lives : int = 5;
	public var walls : int = 6;

	public var InstructionsPanel : GameObject;
	public var StartGamePanel : GameObject;
	public var Mentors : Mentor;
	public var stack = new Array(String);

	private var wallIndex : int = 0;
	public var enemySpawnInterval : float = 12;
	private var xMin : float;
	private var xMax : float;
	private var yMin : float;
	private var yMax : float;

	private var hasWall : boolean = false;
	private var isDead : boolean = false;

	private var password : Password;
	private var spawner : Spawner;
	private var spawnedTexts : List.<GameObject>;

	private var lastInputPassword : String = "";
	private var wrongGuesses : int = 0;

	function Start () {

		stack.pop();

		Cursor.visible = true;
		//Time.timeScale = 1f;
		ResetPanel.SetActive(false);
		enemies = new List.<GameObject>();
		spawnedTexts = new List.<GameObject>();
		passwordInputField.text = "";
		livesText.text = lives.ToString();

		xMin = SpawnPoints.GetComponent(RectTransform).rect.xMin;
		xMax = SpawnPoints.GetComponent(RectTransform).rect.xMax;
		yMin = SpawnPoints.GetComponent(RectTransform).rect.yMin;
		yMax = SpawnPoints.GetComponent(RectTransform).rect.yMax;

		spawner = GetComponent(Spawner);
		password = new Password();

		Mentors = GetComponentInChildren(typeof(Mentor), true);
		//InvokeRepeating("SpawnEnemy", 2, enemySpawnInterval);
	}

	function Update () {

/*
		if(Input.GetKeyDown(KeyCode.Space))
		{
			//print(stack.pop());
			for(var i = 0; i < stack.length; i++)
			{
				print(stack[i]);
			}
		}


		if(Input.GetKeyDown(KeyCode.S))
		{
			SavePasswords();
		}

		if(Input.GetKeyDown(KeyCode.L))
		{
			LoadPasswords();
		}
		*/

	}	

	public function StartGame()
	{
		InstructionsPanel.SetActive(false);
		StartGamePanel.SetActive(true);
		passwordInputField.ActivateInputField();
		InvokeRepeating("SpawnEnemy", 2, enemySpawnInterval);
	}

	function SpawnEnemy()
	{
		StartCoroutine(SpawnEnemyCoroutine());
	}

	public function OnEnterPassword()
	{

		lastInputPassword = passwordInputField.text;
		if(lastInputPassword.Equals(""))
		{
			passwordInputField.text = "";
			passwordInputField.ActivateInputField();
			return;
		}


		var day = "";
		var pass = password.RunPassword(lastInputPassword);


		/*
		if(lastInputPassword in stack)
		{
			//print("exists");
			day = "EXISTING!\nTry another";
		}
		*/

		if(IsPasswordInStack(lastInputPassword))
		{
			day = "EXISTING!\nTry another";
		}
		else
		{
			switch (pass) {
			    case "Very Weak":
			        day = "Very Weak";
			        //AddPoint(day);
			        wrongGuesses++;
			        break;
			    case "Weak":
			        day = "Weak";
			        wrongGuesses++;
			        //AddPoint(day);
			        break;
			    case "Average":
			        //day = "Average";
			        day = "Weak";
			        wrongGuesses++;
			        //RemovePoint(day);
			        break;
			    case "Strong":
			        day = "Strong";
			        BuildWall();
			        //RemovePoint(day);
			        break;
			    case "Very Strong":
			        //day = "Very Strong";
			        day = "Strong";
			        //RemovePoint(day);
			        BuildWall();
			        break;
			    case "Secure":
			        //day = "Secure";
			        day = "Strong";
			        //RemovePoint(day);
			        BuildWall();
			        break;
			    case "Very Secure":
			        //day = "Very Secure";
			        day = "Strong";
			        //RemovePoint(day);
			        BuildWall();
			        break;
			}
		}

		passwordInputField.ActivateInputField();
		passwordInputField.text = "";
		StartCoroutine(SpawnText( day ));
		//print(day);
	}

	private function IsPasswordInStack( password : String)
	{
		for(var i = 0; i < stack.length; i++)
		{
			if(password.Equals(stack[i]))
			{
				return true;
			}
		}
		return false;
	}

	private function SpawnText( text : String)
	{
		var go : GameObject = ObjectPool.instance.GetObjectForType("SpawnText", false);
		go.transform.SetParent(transform.root, false);
		go.GetComponentInChildren(Text).text = text;
		spawnedTexts.Add(go);
		yield WaitForSeconds(1);

		ObjectPool.instance.PoolObject(go);
		spawnedTexts.Remove(go);
	}

	private function BuildWall()
	{
		if(wallIndex < brickwalls.Count)
		{
			brickwalls[wallIndex].SetActive(true);
			wallIndex++;
			stack.push(lastInputPassword);
			hasWall = true;
			if(wallIndex == walls)
			{
				Won();
			}
		}
	}

	private function DestroyWall()
	{
		if(wallIndex > 0)
		{
			wallIndex--;
			brickwalls[wallIndex].SetActive(false);
			stack.pop();
			if(wallIndex == 0)
			{
				hasWall = false;
			}
		}
		else
		{
			hasWall = false;
		}
	}

	public function SpawnEnemyCoroutine()
	{
		var spawnPosition : Vector2 = new Vector2(Random.Range (xMin, xMax), Random.Range (yMin, yMax));
		var spawnRotation : Quaternion = Quaternion.identity;

		var randomEnemy : int = Random.Range(0,100);

		var go : GameObject;


		/*
		if(randomEnemy >= 70)
		{
			go = ObjectPool.instance.GetObjectForType("BlueMonster", false);
		}
		else if(randomEnemy >= 40)
		{
			go = ObjectPool.instance.GetObjectForType("OrangeMonster", false);
		}
		else
		{
			go = ObjectPool.instance.GetObjectForType("PurpleMonster", false);
		}
		*/

		go = ObjectPool.instance.GetObjectForType("OrangeMonster", false);

		go.transform.localPosition = spawnPosition;
		go.transform.localRotation = spawnRotation;
		go.transform.SetParent(SpawnPoints.transform.parent.parent, false);
		enemies.Add(go);
		//print(spawnPosition);
		StartCoroutine(LerpSpawnedEnemy(go));

		yield;
	}

	function LerpSpawnedEnemy(go : GameObject)
	{

/*
		var distance = Vector3.Distance(go.transform.position, BrickPoint.transform.position);

		//print("Start 1");
		while (distance > 1f)
     	{
     		
			var step = speed * Time.deltaTime;
			go.transform.position = Vector3.MoveTowards(go.transform.position, BrickPoint.transform.position, step);
			distance = Vector3.Distance(go.transform.position, BrickPoint.transform.position);
     		
         	yield;
     	}
     	//print("End 1");

     	yield;
*/

     	//StartCoroutine (MoveToPosition (go.transform, BrickPoint.transform.position, 5f));
     	var currentPos = go.transform.GetComponent(RectTransform).position ; //transform.position;
     	var position = BrickPoint.transform.position;
		var t = 0f;
		var timeToMove = speed;

		while(t < 1)
		{
			t += Time.deltaTime / timeToMove;
			var newPos : Vector3 = Vector3.Lerp(currentPos, position, t);
			go.transform.GetComponent(RectTransform).position = newPos;

			yield null;
		}


     	if(hasWall)
     	{
			enemies.Remove(go);
			DestroyWall();
			ObjectPool.instance.PoolObject(go);
			yield WaitForEndOfFrame ();
			return;
     	}
     
     	//print("Start 2");
 /*

		distance = Vector3.Distance(go.transform.position, EndPoint.transform.position);
		step = 0;

		while (distance > 1f)
     	{
			step = speed * Time.deltaTime;
			go.transform.position = Vector3.MoveTowards(go.transform.position, EndPoint.transform.position, step);
			distance = Vector3.Distance(go.transform.position, EndPoint.transform.position);
         	yield;
     	}
     	//print("End 2");

  */
     	//StartCoroutine (MoveToPosition (go.transform, EndPoint.transform.position, 3f));
     	currentPos = go.transform.GetComponent(RectTransform).position ; //transform.position;
     	position = EndPoint.transform.position;
		t = 0f;
		timeToMove = 2f;

		while(t < 1)
		{
			t += Time.deltaTime / timeToMove;
			var newPos2 : Vector3 = Vector3.Lerp(currentPos, position, t);
			go.transform.GetComponent(RectTransform).position = newPos2;

			yield null;
		}


     	RemoveLife();
     	enemies.Remove(go);
     	ObjectPool.instance.PoolObject(go);

		yield WaitForEndOfFrame ();
	}



	function MoveToPosition(transform : Transform, position : Vector3, timeToMove : float)
	{
		var currentPos = transform.GetComponent(RectTransform).position ; //transform.position;
		var t = 0f;
		while(t < 1)
		{
			t += Time.deltaTime / timeToMove;
			var newPos : Vector3 = Vector3.Lerp(currentPos, position, t);
			transform.GetComponent(RectTransform).position = newPos;

			yield null;
		}
	}


	//StartCoroutine (MoveOverSeconds (gameObject, new Vector3 (0.0f, 10f, 0f), 5f));
	function MoveOverSpeed (objectToMove : GameObject , end : Vector3, speed : float){
     	// speed should be 1 unit per second
		while (objectToMove.transform.position != end)
     	{
			objectToMove.transform.position = Vector3.MoveTowards(objectToMove.transform.position, end, speed * Time.deltaTime);
			yield WaitForEndOfFrame ();
		}
	}

	function MoveOverSeconds (objectToMove : GameObject , end : Vector3, seconds : float)
	{
		var elapsedTime : float = 0;
		var startingPos : Vector3 = objectToMove.transform.position;
		while (elapsedTime < seconds)
		{
			transform.position = Vector3.Lerp(startingPos, end, (elapsedTime / seconds));
			elapsedTime += Time.deltaTime;
			yield WaitForEndOfFrame();
		}
		transform.position = end;
	}


	private function RemoveLife()
	{
		if(lives > 0)
		{
			lives--;

			livesText.text = lives.ToString();
			if(lives == 0)
			{
				isDead = true;
				print("isDead " + isDead);
				Dead();
			}
		}
		else
		{
			isDead = true;
			print("isDead " + isDead);
			Dead();
		}

		wrongGuesses++;
	}

	private function SavePasswords()
	{
		var savedPass : String[] = new String[stack.length]; // = stack.ToArray();
		//savedPass = stack;
		for(var i = 0; i < stack.length; i++)
		{
			savedPass[i] = stack[i].ToString();
			print(savedPass[i]);
		}

		PlayerPrefsX.SetStringArray("passwords", savedPass);
	}

	private function LoadPasswords()
	{
		var loadedPasswords = PlayerPrefsX.GetStringArray("passwords");
		for(var i = 0; i < loadedPasswords.length; i++)
		{
			print(loadedPasswords[i]);
		}
	}

	private function Won()
	{
		passwordInputField.interactable = false;
		StopAllCoroutines();
		CancelInvoke();
		WonPanel.SetActive(true);
		Mentors.gameState = Mentors.GameState.DefenceGame;
		Mentors.ShowTip(Random.Range(0, Mentors.Mentors.Length), wrongGuesses);
		ClearSpawnedTexts();
		SavePasswords();
	}

	private function Dead()
	{
		passwordInputField.interactable = false;
		StopAllCoroutines();
		CancelInvoke();
		ResetPanel.SetActive(true);
		ClearSpawnedTexts();
		StartGamePanel.SetActive(false);
	}

	private function Reset()
	{
		lives = 5;
		livesText.text = lives.ToString();
		wallIndex = 0;

		for(var go in enemies)
		{
			ObjectPool.instance.PoolObject(go);
		}
		enemies.Clear();

		ClearSpawnedTexts();

		for(var wall in brickwalls)
		{
			wall.SetActive(false);
		}

	}

	private function ClearSpawnedTexts()
	{
		for(var st in spawnedTexts)
		{
			ObjectPool.instance.PoolObject(st);
		}
		spawnedTexts.Clear();
	}

	public function OnRestart()
	{
		Time.timeScale = 1f;
		Reset();
		ResetPanel.SetActive(false);
		WonPanel.SetActive(false);
		passwordInputField.text = "";
		passwordInputField.interactable = true;
		InstructionsPanel.SetActive(true);
		//InvokeRepeating("SpawnEnemy", 2, enemySpawnInterval);
	}

	public function OnMainMenu()
	{
		SceneManager.LoadScene("00.MainMenu");
	}

	public function OnNext()
	{
		SceneManager.LoadScene("03.RememberPassword");
	}

}