﻿#pragma strict

import System.Collections.Generic;

class Spawner extends MonoBehaviour
{
	public var PasswordPrefab : GameObject;
	public var ScreenPoints : Transform[];
	public var usedPoints : List.<Transform>;
	public var spawnedPasswords : List.<GameObject>;

	public var BlockInputPanel : GameObject;

	private var xMin : float;
	private var xMax : float;
	private var yMin : float;
	private var yMax : float;
	private var passwordsPanel : Transform;

	function Awake()
	{

	}

	// Use this for initialization
	function Start () {
		usedPoints = new List.<Transform>();
		spawnedPasswords = new List.<GameObject>();
		passwordsPanel = transform.FindChild("PasswordsPanel");
		BlockInputPanel.SetActive(true);

		xMin = passwordsPanel.GetComponent(RectTransform).rect.xMin;
		xMax = passwordsPanel.GetComponent(RectTransform).rect.xMax;
		yMin = passwordsPanel.GetComponent(RectTransform).rect.yMin;
		yMax = passwordsPanel.GetComponent(RectTransform).rect.yMax;
	}

	public function SpawnPassword( password : String)
	{
		var spawnPosition : Vector2 = new Vector2(Random.Range (xMin, xMax), Random.Range (yMin, yMax));
		var spawnRotation : Quaternion = Quaternion.identity;
/*
		var go : GameObject = Instantiate(PasswordPrefab);
		go.transform.localPosition = spawnPosition;
		go.transform.localRotation = spawnRotation;
		go.transform.SetParent(passwordsPanel, false);
		*/
		var go : GameObject = ObjectPool.instance.GetObjectForType("ButtonPassword", false);
		go.transform.localPosition = spawnPosition;
		go.transform.localRotation = spawnRotation;
		go.transform.SetParent(passwordsPanel, false);
		go.GetComponentInChildren(Text).text = password; //wordDictionary.GetRandomWord();
		StartCoroutine(LerpSpawnedObj(go));

		yield;
	}

	function LerpSpawnedObj(go : GameObject)
	{
		var time : float = 1.5f;
		var randomPos : Transform = ScreenPoints[Random.Range(0, ScreenPoints.Length)];
		var elapsedTime : float = 0;
     	//var startingPos : Vector3 = go.transform.position;

     	var breakCounter = 0;

     	if(usedPoints.Contains(randomPos))
     	{
	     	while(usedPoints.Contains(randomPos))
	     	{
	     		randomPos = ScreenPoints[Random.Range(0, ScreenPoints.Length)];  //ScreenPoints.Length-1)
	     		if(!usedPoints.Contains(randomPos))
	     		{
	     			usedPoints.Add(randomPos);
	     			break;
	     		}
	     		if(breakCounter > 30)
	     		{	
	     			break;
	     		}
	     		breakCounter++;
	     	}
     	}
     	else
     	{
     		usedPoints.Add(randomPos);
     	}

     	var randomOffsetX : float = 0;
     	if(go.GetComponentInChildren(Text).preferredWidth < 200)
     	{
     		randomOffsetX = go.GetComponentInChildren(Text).preferredWidth / 2f;
     	}
     	else
     	{
     		randomOffsetX = go.GetComponentInChildren(Text).preferredWidth / 5f;
     	}

     	var randOffset : Vector3 = new Vector3(Random.Range (-randomOffsetX, randomOffsetX),0,0);

     	//print(randomOffsetX + "    " + randOffset);

     	//go.transform.SetParent(randomPos, false);
     	while (elapsedTime < time)
     	{
			go.transform.localPosition = Vector3.Lerp(go.transform.localPosition, randomPos.localPosition + randOffset, (elapsedTime / time)); 
			elapsedTime += Time.deltaTime;
         	yield;
     	}
	}


	public function Reset()
	{
		for(var i = spawnedPasswords.Count-1; i >= 0; i--)
		{
			ObjectPool.instance.PoolObject(spawnedPasswords[i]);
			//spawnedPasswords[i].gameObject.SetActive(false);
		}
		BlockInputPanel.SetActive(true);
	}

	public function AddToSpawner( go : GameObject)
	{
		spawnedPasswords.Add(go);
	}

	public function RemoveFromSpawner( go : GameObject)
	{
		spawnedPasswords.Remove(go);
		//usedPoints.Remove(go.transform);
	}
}