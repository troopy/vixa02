﻿#pragma strict

import System.IO;
import System.Collections.Generic;
import System.Linq;

var fileLines : List.<String>;

class WordDictionary extends MonoBehaviour
{
	var filename = "wordsAll.txt";
	//var path = String.Format("Assets/StreamingAssets/{0}", filename);
	var path = ""; 
	var result = "";

	var words : TextAsset; 

	function Start () {

		//path = String.Format(Application.streamingAssetsPath + "/" + "{0}", filename);
//	#if UNITY_EDITOR
//	#else
		
//	#endif
		//ReadFile();

		//words = Resources.Load("wordsAll") as TextAsset;

		ReadTextAsset();
		//DeleteShortWords();
	}

	private function ReadTextAsset()
	{
		fileLines = words.text.Split("\n"[0]).ToList();
	}

	private function ReadFile() {
		//print(path);
		if (path.Contains("://")) {
            var www = new WWW(path);
            yield www;
            result = www.text;
        } else
            result = System.IO.File.ReadAllText(path);

        fileLines = result.Split("\n"[0]).ToList();

        result = null;
		/*
		var sr = File.OpenText(path);
		fileLines = sr.ReadToEnd().Split("\n"[0]).ToList();
		sr.Close();
		*/
   }
	
   function GetRandomWord()
   {
   		var rand = Random.Range(0, fileLines.Count-1);
		var word = fileLines[rand];

		return word;
   }

   private function DeleteShortWords()
   {
   		for(var i = fileLines.Count-1; i >= 0; i--)
   		{
   			if(i % 2 == 0)
   			{
   				fileLines[i] = null;
   				fileLines.RemoveAt(i);
   			}
   		}
   		WriteFile(path);
   }

   private	function WriteFile(filepathIncludingFileName : String)
	{
		var sw : StreamWriter = new StreamWriter(filepathIncludingFileName);
	    var wholeFileText = String.Join("\n", fileLines.ToArray());

	  	sw.Write(wholeFileText);
	    sw.Flush();
	    sw.Close();
	}
}

   
//	function ReadFile()
//	{
//		var filename = "words.txt";
//	#if UNITY_EDITOR
//	    var dbPath = string.Format(@"Assets/StreamingAssets/{0}", filename);
//	#else
//	    // check if file exists in Application.persistentDataPath
//	    var filepath = string.Format("{0}/{1}", Application.persistentDataPath, filename);
//
//		if (!File.Exists(filepath))
//	    {
//	        Debug.Log("Database not in Persistent path");
//	        // if it doesn't ->
//	        // open StreamingAssets directory and load the db ->
//	        var loadDb = Application.streamingAssetsPath + "/" + filename;  // this is the path to your StreamingAssets in iOS
//			// then save to Application.persistentDataPath
//			File.Copy(loadDb, filepath);
//
//	    }
//
//	}
