﻿#pragma strict
/*

Password Length:
    5 Points: Less than 4 characters
    10 Points: 5 to 7 characters
    25 Points: 8 or more

Letters:
    0 Points: No letters
    10 Points: Letters are all lower case
    20 Points: Letters are upper case and lower case

Numbers:
    0 Points: No numbers
    10 Points: 1 number
    20 Points: 3 or more numbers

Characters:
    0 Points: No characters
    10 Points: 1 character
    25 Points: More than 1 character

Bonus:
    2 Points: Letters and numbers
    3 Points: Letters, numbers, and characters
    5 Points: Mixed case letters, numbers, and characters

Password Text Range:

    >= 90: Very Secure
    >= 80: Secure
    >= 70: Very Strong
    >= 60: Strong
    >= 50: Average
    >= 25: Weak
    >= 0: Very Weak

*/

class Password
{
	var m_strUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	var m_strLowerCase = "abcdefghijklmnopqrstuvwxyz";
	var m_strNumber = "0123456789";
	var m_strCharacters = "!@#$%^&*?_~";

	function checkPassword(strPassword : String)
	{
	    // Reset combination count
	    var nScore = 0;

	    // Password length
	    // -- Less than 4 characters
	    if (strPassword.length < 5)
	    {
	        nScore += 5;
	    }
	    // -- 5 to 7 characters
	    else if (strPassword.length > 4 && strPassword.length < 8)
	    {
	        nScore += 10;
	    }
	    // -- 8 or more
	    else if (strPassword.length > 7)
	    {
	        nScore += 25;
	    }

	    // Letters
	    var nUpperCount = countContain(strPassword, m_strUpperCase);
	    var nLowerCount = countContain(strPassword, m_strLowerCase);
		var nLowerUpperCount = nUpperCount + nLowerCount;

	    // -- Letters are all lower case
	    if (nUpperCount == 0 && nLowerCount != 0) 
	    { 
	        nScore += 10; 
	    }
	    // -- Letters are upper case and lower case
	    else if (nUpperCount != 0 && nLowerCount != 0) 
	    { 
	        nScore += 20; 
	    }

	    // Numbers
	    var nNumberCount = countContain(strPassword, m_strNumber);
	    // -- 1 number
	    if (nNumberCount == 1)
	    {
	        nScore += 10;
	    }
	    // -- 3 or more numbers
	    if (nNumberCount >= 3)
	    {
	        nScore += 20;
	    }

	    // Characters
	    var nCharacterCount = countContain(strPassword, m_strCharacters);
	    // -- 1 character
	    if (nCharacterCount == 1)
	    {
	        nScore += 10;
	    }   
	    // -- More than 1 character
	    if (nCharacterCount > 1)
	    {
	        nScore += 25;
	    }

	    // Bonus
	    // -- Letters and numbers
	    if (nNumberCount != 0 && nLowerUpperCount != 0)
	    {
	        nScore += 2;
	    }
	    // -- Letters, numbers, and characters
	    if (nNumberCount != 0 && nLowerUpperCount != 0 && nCharacterCount != 0)
	    {
	        nScore += 3;
	    }
	    // -- Mixed case letters, numbers, and characters
	    if (nNumberCount != 0 && nUpperCount != 0 && nLowerCount != 0 && nCharacterCount != 0)
	    {
	        nScore += 5;
	    }

	    return nScore;
	}

	// Runs password through check and then updates GUI 
	public function RunPassword(strPassword : String) 
	{
	    // Check password
	    var nScore = checkPassword(strPassword);

	    // Color and text
	    var strText = "";
	    //var strColor = "000000";

	    // -- Very Secure
	    if (nScore >= 90)
	    {
	        strText = "Very Secure";
	       // strColor = "#0ca908";
	    }
	    // -- Secure
	    else if (nScore >= 80)
	    {
	        strText = "Secure";
	        //strColor = "#7ff67c";
	    }
	    // -- Very Strong
	    else if (nScore >= 80)
	    {
	        strText = "Very Strong";
	       // strColor = "#008000";
	    }
	    // -- Strong
	    else if (nScore >= 60)
	    {
	        strText = "Strong";
	       // strColor = "#006000";
	    }
	    // -- Average
	    else if (nScore >= 40)
	    {
	        strText = "Average";
	        //strColor = "#e3cb00";
	    }
	    // -- Weak
	    else if (nScore >= 20)
	    {
	        strText = "Weak";
	       // strColor = "#Fe3d1a";
	    }
	    // -- Very Weak
	    else
	    {
	        strText = "Very Weak";
	       // strColor = "#e71a1a";
	    }

	    return strText;
	}

	// Checks a string for a list of characters
	function countContain(strPassword : String, strCheck : String)
	{ 
	    var nCount = 0;
	     
	    for (var i = 0; i < strPassword.length; i++) 
	    {
	        if (strCheck.IndexOf(strPassword[i]) > -1) 
	        { 
	                nCount++;
	        } 
	    } 

	    return nCount; 
	}

	public function GenerateVeryWeakPass( word1 : String, word2 : String )
   {
   		var minChars : int = Random.Range(0, 100);

   		var	output = word1;
   		if(minChars >= 80)
   		{
   			output += word2;
   		}
   		else if(minChars >= 30)
   		{
   			output = RandomUppercase(output);
   		}
   		else
   		{
   			output = AddStartEndNumber(word1);
   		}

   		return output;
   }

   public function GenerateWeakAveragePass( output : String )
   {
   		var minChars : int = Random.Range(8, 12);
   		var wordLength = output.Length;
   		//var output = word;

   		while(output.Length < minChars)
   		{
   			output = RandomCase(output, 101, 101, 60, 30);
   		}
   		return output;
   }

   public function GenerateVeryStrongPass( output : String )
   {
   		var minChars : int = Random.Range(10, 14);
   		var wordLength = output.Length;
   		//var output = word;

   		while(output.Length < minChars)
   		{
   			output = RandomCase(output, 85, 55, 35, 5);
   		}
   		return output;
   }

   public function GenerateVerySecuredPass( output : String )
   {
   		var minChars : int = 16;
   		var wordLength = output.Length;
   		//var output = word;

   		while(output.Length < minChars)
   		{
   			output = RandomCase(output, 70, 40, 30, 15);
   		}
   		return output;
   }

   private function RandomCase( word : String, symbol : int, number : int, letter : int, uppercase : int)
   {
   		var random : int = Random.Range(0, 100);

   		if(random >= symbol) //80
   		{
   			word = AddRandomSymbol(word);
   		}
   		else if(random >= number) //60
   		{
   			word = AddRandomNumber(word);
   		}
   		else if(random >= letter) //40
   		{
   			word = AddRandomLetter(word);
   		}
   		else if(random >= uppercase) //20
   		{
   			word = RandomUppercase(word);
   		}
   		else
   		{
   			word = AddStartEndNumber(word);
   			//print("else");
   		}

   		return word;
   }

   private function RandomUppercase( word : String)
   {
   		var randomLetter = Random.Range(0, word.Length);
   		var letter = word[randomLetter].ToString().ToUpper(); 
   		word = word.Replace(word[randomLetter].ToString(), letter);
   		//print(word);
   		return word;
   }

   private function AddStartEndNumber(word : String)
   {
   		var position = Random.Range(0, 2);
   		if(position == 0)
   			position = 0;
   		else
   			position = word.Length;

   		var randomNumber = Random.Range(0, 9).ToString();
		var output = word.Substring(0, position) + randomNumber + word.Substring(position);
   		//print(output);
   		return output;
   }

    private function AddRandomLetter(word : String)
   {
   		var letters = "abcdefghijklmnopqrstuvwxyz";
   		var position = Random.Range(0, word.Length);

   		var randomSymbol = letters[Random.Range(0, letters.Length)];
   		var output = word.Substring(0, position) + randomSymbol + word.Substring(position);
   		return output;
   }

   private function AddRandomSymbol(word : String)
   {
   		var symbols = "!@#$%^&*?_~";
   		var position = Random.Range(0, word.Length);

   		var randomSymbol = symbols[Random.Range(0, symbols.Length)];
   		var output = word.Substring(0, position) + randomSymbol + word.Substring(position);
   		return output;
   }

   private function AddRandomNumber(word : String)
   {
   		var numbers = "1234567890";
   		var position = Random.Range(0, word.Length);

   		var randomSymbol = numbers[Random.Range(0, numbers.Length)];
   		var output = word.Substring(0, position) + randomSymbol + word.Substring(position);
   		return output;
   }

	private function GetMD5HashString(unhashed : String) : String
	{
        Debug.Log("Generating MD5 hash value from: \"" + unhashed + "\"");
        var inputBytes : byte[] = System.Text.Encoding.ASCII.GetBytes(unhashed);
        var md5 : System.Security.Cryptography.MD5 = System.Security.Cryptography.MD5.Create();
        var hash : byte[] = md5.ComputeHash(inputBytes);
        var sb : System.Text.StringBuilder = new System.Text.StringBuilder();
        for (var i : int = 0; i < hash.Length; i++)
        {
            sb.Append(hash[i].ToString("x2"));
        }
        return sb.ToString();
	} 


}
