﻿#pragma strict

import UnityEngine.EventSystems;
import UnityEngine.UI;

class OnPasswordButtonPressed extends MonoBehaviour implements IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler
{
	var spawner : Spawner;
	var passwordGame : PasswordGame;
	var go : GameObject;
	private var password : Password;

	function OnEnable () {
		//print("OnEnable");
		if (spawner == null)
		{
			go = GameObject.FindObjectOfType(Spawner).gameObject; 
			if(go != null)
			{
				spawner = go.GetComponent(Spawner);
				spawner.AddToSpawner(gameObject);
				passwordGame = go.GetComponent(PasswordGame);
			}
		}
		else
		{
			spawner.AddToSpawner(gameObject);
		}
	}

	function OnDisable()
	{
		//print("OnDisable");
		if(go == null)
		{
			print("spawner is null");
			return;
		}

		spawner.RemoveFromSpawner(gameObject);

		if(spawner != null)
		{
			if(spawner.usedPoints.Count > 0)
			{
				spawner.usedPoints.RemoveAt(0);
			}
		}
		//ObjectPool.instance.PoolObject(gameObject);
	}

	function Start () {

		password = new Password();

		//print("OnStart");
	}

	public function OnPointerDown (eventData : PointerEventData)
	{
		if(go == null)
		{
			print("spawner is null");
			return;
		}
			
		//Debug.Log(gameObject.name +  " clicked");
		GetComponent(Image).enabled = false;
		//print(eventData);
		//spawner.RemoveFromSpawner(gameObject);
		passwordGame.CheckPassword(GetComponentInChildren(Text).text);
		//passwordGame.Reset();


		//ObjectPool.instance.PoolObject(gameObject);
		//throw new System.NotImplementedException ();
	}

	public function OnPointerEnter (eventData : PointerEventData)
	{
		//Debug.Log(GetComponentInChildren(Text).text);
		GetComponent(Image).enabled = true;
	}

	public function OnPointerExit (eventData : PointerEventData)
	{
		GetComponent(Image).enabled = false;
	}

}