﻿#pragma strict
import UnityEngine.EventSystems;
import UnityEngine.UI;
import UnityEngine.SceneManagement;

class PasswordGame extends MonoBehaviour
{
	public var target : GameObject;

	public var bundleUrl : String;

	var assetBundle : AssetBundle;

	public var numberOfPasswords : int = 10;
	//public var pathText : Text;
	public var numberOfWeakPasswords : int = 2;

	public var pointValue : int = 1;
	public var pointPenalty : int = 1;

	public var passScoreText : Text;
	public var passStrengthText : Text;

	public var StartButton : GameObject;
	public var wonPanel : GameObject;

	public var Mentors : Mentor;

	public var PointsToWin : int = 5;
	private var points : int;

	private var wordDictionary : WordDictionary;
	var spawner : Spawner;
	private var password : Password;
	private var hasWon : boolean = false;

	private var spawnedTexts : List.<GameObject>;

	private var wrongGuesses : int = 0;

	function Awake()
	{
		points = 0; 
	}

	function Start () {
		//Time.timeScale = 1f;
		spawnedTexts = new List.<GameObject>();
		wordDictionary = GetComponent(WordDictionary);
		//spawner = GetComponent(Spawner);
		password = new Password();
		//pathText.text = wordDictionary.path;
		Cursor.visible = false;

		Mentors = GetComponentInChildren(typeof(Mentor), true);
		//Mentors = GameObject.FindObjectOfType(Mentor);

		//print(Mentors);
	}

	function Update () {

		target.transform.position = Input.mousePosition;

		/*
		if(Input.GetKeyDown(KeyCode.Space))
		{
			StartCoroutine(SpawnPasswords());
		}
		*/
	}

	public function StartGame()
	{
		StartButton.SetActive(false);

		StartCoroutine(SpawnPasswords());
	}

	public function SpawnPasswords()
	{
		var countWeakPasswords : int = 0;
		yield WaitForSeconds(0.5f);
		for(var i = 0; i < numberOfPasswords; i++)
		{
			if(countWeakPasswords < numberOfWeakPasswords)
			{
				StartCoroutine(spawner.SpawnPassword(SpawnVeryWeakPassword()));
				countWeakPasswords++;
			}
			else
			{
				var rand = Random.Range(0, 100);
				if(rand >= 90)
				{
					StartCoroutine(spawner.SpawnPassword(SpawnSecurePassword()));
				}
				else if(rand >= 60)
				{
					StartCoroutine(spawner.SpawnPassword(SpawnStrongPassword()));
				}
				else if(rand >= 30)
				{
					StartCoroutine(spawner.SpawnPassword(SpawnWeakPassword()));
				}
				else
				{
					StartCoroutine(spawner.SpawnPassword(SpawnVeryWeakPassword()));
				}
			}

			yield WaitForSeconds(0.1f);
		}

		yield WaitForSeconds(0.4f);
		spawner.BlockInputPanel.SetActive(false);
	}

	private function SpawnVeryWeakPassword()
	{
		var pass = password.GenerateVeryWeakPass(wordDictionary.GetRandomWord(), wordDictionary.GetRandomWord());
		return pass;
	}

	private function SpawnWeakPassword()
	{
		var pass = password.GenerateWeakAveragePass(wordDictionary.GetRandomWord());
		return pass;
	}

	private function SpawnStrongPassword()
	{
		var pass = password.GenerateVeryStrongPass(wordDictionary.GetRandomWord());
		return pass;
	}

	private function SpawnSecurePassword()
	{
		var pass = password.GenerateVerySecuredPass(wordDictionary.GetRandomWord());
		return pass;
	}

	public function AddPoint(text : String)
	{
		points += pointValue;
		UIShowPassScore(points.ToString(), text);

		if(points == PointsToWin)
		{
			hasWon = true;
		}

	}

	public function RemovePoint(text : String)
	{
		var tempPoint : int = points - pointPenalty;
		if(tempPoint < 0)
		{
			points = 0;
		}
		else
		{
			points = tempPoint;
		}

		wrongGuesses++;

		UIShowPassScore(points.ToString(), text);
	}

	public function CheckPassword( pass : String)
	{
    	var day = "";

    	pass = password.RunPassword(pass);

		switch (pass) {
		    case "Very Weak":
		        day = "Very Weak";
		        AddPoint(day);
		        break;
		    case "Weak":
		        day = "Weak";
		        AddPoint(day);
		        break;
		    case "Average":
		        //day = "Average";
		        day = "Weak";
		        AddPoint(day);
		        break;
		    case "Strong":
		        day = "Strong";
		        RemovePoint(day);
		        break;
		    case "Very Strong":
		        //day = "Very Stron";
		        day = "Strong";
		        RemovePoint(day);
		        break;
		    case "Secure":
		        //day = "Secure";
		        day = "Strong";
		        RemovePoint(day);
		        break;
		    case "Very Secure":
		        //day = "Very Secure";
		        day = "Strong";
		        RemovePoint(day);
		        break;
		}
		//print(day);
		//StartCoroutine(SpawnText( day ));

		CheckHasWon();

	}

	private function SpawnText( text : String)
	{
		var go : GameObject = ObjectPool.instance.GetObjectForType("SpawnText", false);
		go.transform.SetParent(ObjectPool.instance.transform, false);
		go.GetComponentInChildren(Text).text = text;
		spawnedTexts.Add(go);
		yield WaitForSeconds(1);

		ObjectPool.instance.PoolObject(go);
		spawnedTexts.Remove(go);
	}

	function CheckHasWon()
	{
		Reset();
	
		if(hasWon)
		{
			
			//	winning text
			spawner.BlockInputPanel.SetActive(true);
			wonPanel.SetActive(true);
			Mentors.gameState = Mentors.GameState.PasswordGame;
			Mentors.ShowTip(Random.Range(0, Mentors.Mentors.Length), wrongGuesses);
			//wonPanel.GetComponent<Animator>()

		}
		else
		{
			StartCoroutine(SpawnPasswords());
		}
	}

	function UIShowPassScore( score : String, strength : String)
	{
		passScoreText.text = score;

		StartCoroutine(SpawnText( strength ));
		
		//passStrengthText.text = strength;
	}

	private function ClearSpawnedTexts()
	{
		for(var st in spawnedTexts)
		{
			ObjectPool.instance.PoolObject(st);
		}
		spawnedTexts.Clear();
	}

	// Reset passwords
	public function Reset()
	{
		//StopAllCoroutines();
		spawner.Reset();
	}

	public function OnNextLevel()
	{
		SceneManager.LoadScene("02.DefenceGame");
	}

	public function OnMainMenu()
	{
		SceneManager.LoadScene("00.MainMenu");
	}
}