﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class SpawnerCS : MonoBehaviour {

	public GameObject PasswordPrefab;
	public int numberOfPasswords = 10;

	public GameObject[] spawnPoints;
	public List<GameObject> spawnedPasswords;

	private int points;
	public int pointValue = 1;
	public int pointPenalty = 1;

	void Awake()
	{
		points = 0;
	}

	// Use this for initialization
	void Start () {
		spawnedPasswords = new List<GameObject>();
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.GetMouseButtonDown(0))
		{
			StartCoroutine(SpawnPassword());
		}	

	}

	public IEnumerator SpawnPassword()
	{
		//spawnedPasswords.Contains();
		yield return new WaitForSeconds(0.5f);
		Vector3 pos = spawnPoints[Random.Range(0, spawnPoints.Length-1)].transform.position;

		GameObject go = (GameObject) Instantiate(PasswordPrefab, pos, Quaternion.identity);
		go.transform.SetParent(transform, false); 
	//	go.GetComponent<LayoutElement>().fl
		//go.transform.GetChild(0)
		//string s = "";
		//File.op
		
	}

	public void AddPoint()
	{
		points += pointValue;
	}

	public void RemovePoint()
	{
		int tempPoint = points - pointPenalty;
		if(tempPoint < 0)
		{
			points = 0;
		}
		else
		{
			points = tempPoint;
		}
	}

	public void AddToSpawner(GameObject go)
	{
		spawnedPasswords.Add(go);
	}

	public void RemoveFromSpawner(GameObject go)
	{
		spawnedPasswords.Remove(go);
	}



}
