﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class UIManagerCS : MonoBehaviour {

	public GameObject MenuPanel;

	// Use this for initialization
	void Start () {
		ToggleMenuPanel(true);
		//	MenuPanel = GetComponent<GameObject>();
	}


	public void OnStart()
	{
		ToggleMenuPanel(false);
	}


	void ToggleMenuPanel(bool toggle)
	{
		MenuPanel.SetActive(toggle);
	}

}
