﻿#pragma strict
import UnityEngine.SceneManagement;


public class MainMenu extends MonoBehaviour
{	
	public var MenuPanel : GameObject;

	function Start () {
		Cursor.visible = true;
		ToggleMenuPanel(true);
	}

	public function OnStart()
	{
		SceneManager.LoadScene("01.PasswordGame");
		ToggleMenuPanel(false);
	}

	public function OnGuessPassword()
	{	
		ToggleMenuPanel(false);
		SceneManager.LoadScene("01.PasswordGame");
	}

	public function OnDefendCastle()
	{
		ToggleMenuPanel(false);
		SceneManager.LoadScene("02.DefenceGame");
	}

	private function ToggleMenuPanel(toggle : boolean)
	{
		MenuPanel.SetActive(toggle);
	}


}