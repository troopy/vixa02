﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MenuSelectorCS : MonoBehaviour {

	public GameObject selector;

	public GameObject[] menuItems;

	[SerializeField]
	private GameObject _currentSelected;

	[SerializeField]
	private int selectorCounter = 0;

	public bool canReadKey = false;

	// Use this for initialization
	void Start () {

		//EventSystem = GameObject.Find("EventSystem").GetComponent<UnityEngine.EventSystems>();

		if(menuItems.Length > 0)
		{
			_currentSelected = menuItems[0];
		}
	}
	
	// Update is called once per frame
	void Update () {

		if(canReadKey)
		{
			GetInput();
		}
			

		//selector.transform.position=new Vector3(Input.mousePosition.x, Input.mousePosition.y, selector.transform.position.z);
	}

	void GetInput()
	{


		if(Input.GetKeyDown(KeyCode.DownArrow))
		{
			selectorCounter++;
			if(selectorCounter >= menuItems.Length)
			{
				selectorCounter = 0;
			}

			_currentSelected = menuItems[selectorCounter];
			MoveSelector(_currentSelected);
		}
		else if(Input.GetKeyDown(KeyCode.UpArrow))
		{
			selectorCounter--;

			if(selectorCounter < 0)
			{
				selectorCounter = menuItems.Length-1;
			}

			_currentSelected = menuItems[selectorCounter];
			MoveSelector(_currentSelected);
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow))
		{
			selectorCounter++;
			if(selectorCounter >= menuItems.Length)
			{
				selectorCounter = 0;
			}

			_currentSelected = menuItems[selectorCounter];
			MoveSelector(_currentSelected);
		}
		else if(Input.GetKeyDown(KeyCode.RightArrow))
		{
			selectorCounter--;

			if(selectorCounter < 0)
			{
				selectorCounter = menuItems.Length-1;
			}

			_currentSelected = menuItems[selectorCounter];
			MoveSelector(_currentSelected);
		}

		else if(Input.GetKeyDown(KeyCode.Return))
		{
//			Debug.Log(_currentSelected);
			_currentSelected.GetComponent<Button>().onClick.Invoke();
		}


		/*
		else if(Input.GetKeyDown(KeyCode.LeftArrow))
		{

		}
		else if(Input.GetKeyDown(KeyCode.RightArrow))
		{

		}
		*/
	}

	void MoveSelector(GameObject target)
	{
		
		//selector.transform.position = Vector3.Lerp(selector.transform.position, target.transform.position, 1f * Time.deltaTime);
		selector.transform.position = target.transform.position;
	}

	public void MoveSelectorOnMouseover(GameObject go)
	{
		for(int i = 0; i < menuItems.Length; i++)
		{
			var g = menuItems[i];

			if(g.name.Equals(go.name))
			{
				selectorCounter = i;
				_currentSelected = go;
				//MoveSelector(go);
			}
		}

	

		MoveSelector(_currentSelected);
	}

}
