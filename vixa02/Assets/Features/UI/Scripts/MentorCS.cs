﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MentorCS : MonoBehaviour {

	public Image[] Mentors;
	public string[] MentorTips;
	public string[] MentorGoodTips;
	public string[] MentorHelpTips;

	public Text TipText;
	private int currentTipIndex = 0;

	[SerializeField] private float showTime = 0.5f ;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if(Input.anyKeyDown)
		{
			ShowTip(Random.Range(0, Mentors.Length));
		}

	}

	public void ShowGoodTip(int mentorIndex)
	{
		StopCoroutine(ShowGoodMentorTipCo(mentorIndex));
		StartCoroutine(ShowGoodMentorTipCo(mentorIndex));
	}

	IEnumerator ShowGoodMentorTipCo(int mentorIndex)
	{
		foreach(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}
		Mentors[mentorIndex].gameObject.SetActive(true);

		yield return new WaitForSeconds(showTime);
		TipText.text = GetGoodTip();
		TipText.gameObject.SetActive(true);
	}

	public void ShowHelpTip(int mentorIndex)
	{
		StopCoroutine(ShowHelpMentorTipCo(mentorIndex));
		StartCoroutine(ShowHelpMentorTipCo(mentorIndex));
	}

	IEnumerator ShowHelpMentorTipCo(int mentorIndex)
	{
		foreach(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}
		Mentors[mentorIndex].gameObject.SetActive(true);

		yield return new WaitForSeconds(showTime);

		TipText.text = GetHelpTip();
		TipText.gameObject.SetActive(true);
	}


	public void ShowTip(int mentorIndex)
	{
		StopCoroutine(ShowMentorTip(mentorIndex));
		StartCoroutine(ShowMentorTip(mentorIndex));
	}

	IEnumerator ShowMentorTip(int mentorIndex)
	{
		foreach(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}

		Mentors[mentorIndex].gameObject.SetActive(true);

		yield return new WaitForSeconds(showTime);

//		TipText.text = GetRandomMentorTip();
		TipText.text = GetNextTip();
		TipText.gameObject.SetActive(true);

	}

	private string GetRandomMentorTip(string[] tipsArray)
	{
		return tipsArray[Random.Range(0, tipsArray.Length)];
	}

	private string GetMentorTip(int index)
	{
		if(index > MentorTips.Length)
		{
			index = 0;
			Debug.Log("No such index. Return 0");
		}
		return MentorTips[index];
	}

	private string GetNextTip()
	{
		currentTipIndex++;

		if(currentTipIndex >= MentorTips.Length)
		{
			currentTipIndex = 0;
		}
		return MentorTips[currentTipIndex];
	}

	private string GetGoodTip()
	{
		return GetRandomMentorTip(MentorGoodTips)	;
	}

	private string GetHelpTip()
	{
		return GetRandomMentorTip(MentorHelpTips)	;
	}


}
