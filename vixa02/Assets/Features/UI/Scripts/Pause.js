﻿#pragma strict
import UnityEngine;
import UnityEngine.SceneManagement;
import UnityEngine.UI;

public class Pause extends MonoBehaviour
{
	public var PausePanel : GameObject;
	private var isPaused : boolean = false;

	function Awake()
	{
		Time.timeScale = 1f;
		TogglePausePanel(false);

	}

	function Start () {
	}

	function Update () {

		if(Input.GetKeyDown(KeyCode.Escape))
		{
			OnPause();
		}
	}

	public function OnPause()
	{
		isPaused = !isPaused;
		if(isPaused)
		{
			Time.timeScale = 0f;
			TogglePausePanel(true);
			var passIF : InputField = GameObject.FindObjectOfType(InputField);
			if(passIF)
			{
				passIF.enabled = false;
			}

		}
		else
		{
			OnResume();
		}
	}

	public function OnResume()
	{
		var passIF : InputField = GameObject.FindObjectOfType(InputField);
		if(passIF)
		{
			passIF.enabled = true;
			passIF.ActivateInputField();
		}
//		GameObject.FindObjectOfType(InputField).ActivateInputField();
		isPaused = false;
		TogglePausePanel(false);
		Time.timeScale = 1f;
	}

	public function OnRestart()
	{
		var scene : Scene = SceneManager.GetActiveScene();
		SceneManager.LoadScene(scene.name);
	}

	public function OnMainMenu()
	{
		SceneManager.LoadScene("00.MainMenu");
	}

	function TogglePausePanel( toggle : boolean)
	{
		PausePanel.SetActive(toggle);
	}

}