﻿#pragma strict
import UnityEngine.UI;

class Mentor extends MonoBehaviour
{
	public var Mentors : Image[];
	public var MentorTips : String[];
	public var MentorGoodTips : String[];
	public var MentorHelpTips : String[];

	public var TipText : Text;

	private var currentTipIndex : int = 0;

	private var showTime : float = 0.5;

	private var tipCounter : int = 0;
	private var repeatTip : int = 3;

	private var showEndButtons : boolean = false;

	private var menuButtons : GameObject[]; 

	private var menuSelector : GameObject;

	public enum GameState { PasswordGame, DefenceGame, RememberGame, None}

	public var gameState = GameState.None;

	function Start () {
		menuButtons = gameObject.GetComponentInParent(typeof(MenuSelector)).menuItems;
		menuSelector = gameObject.GetComponentInParent(typeof(MenuSelector)).selector;
	}

	function Update () {


		switch(gameState)
		{
			case GameState.PasswordGame:
				if(!showEndButtons)
				{
					if(repeatTip > 0)
					{
						if(Input.anyKeyDown)
						{
							ShowHelpTip(Random.Range(0, Mentors.Length));
						}
					}
					else
					{
						showEndButtons = true;
						StartCoroutine(ShowMenuButtons());
					}
				}
				break;
			case GameState.DefenceGame:
				if(!showEndButtons)
				{
					if(repeatTip > 0)
					{
						if(Input.anyKeyDown)
						{
							ShowHelpTip(Random.Range(0, Mentors.Length));
						}
					}
					else
					{
						showEndButtons = true;
						StartCoroutine(ShowMenuButtons());
					}
				}
				break;
			case GameState.RememberGame:
				if(!showEndButtons)
				{
					if(repeatTip > 0)
					{
						if(Input.anyKeyDown)
						{
							ShowHelpTip(Random.Range(0, Mentors.Length));
						}
					}
					else
					{
						showEndButtons = true;
						StartCoroutine(ShowMenuButtons());
					}
				}
				break;
			case GameState.None:
				print("None");
				break;
			default:
				print("default");
				//break;
		}



	}

	public function ShowGoodTip(mentorIndex : int)
	{
		StopCoroutine(ShowGoodMentorTipCo(mentorIndex));
		StartCoroutine(ShowGoodMentorTipCo(mentorIndex));
	}

	function ShowMenuButtons()
	{
		yield WaitForSeconds(2f);
		for(var i = 0; i < menuButtons.Length; i++)
		{
			menuButtons[i].SetActive(true);
		}
		menuSelector.SetActive(true);

		yield;
	}

	function ShowGoodMentorTipCo(mentorIndex : int)
	{
		yield WaitForSeconds(1.5f);

		for(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}
		Mentors[mentorIndex].gameObject.SetActive(true);

		yield WaitForSeconds(showTime);
		TipText.text = GetGoodTip();
		TipText.gameObject.SetActive(true);
	}

	function ShowHelpTip(mentorIndex : int)
	{
		StopCoroutine(ShowHelpMentorTipCo(mentorIndex));
		StartCoroutine(ShowHelpMentorTipCo(mentorIndex));
		repeatTip--;
	}

	function ShowHelpMentorTipCo(mentorIndex : int)
	{
		yield WaitForSeconds(1.5f);

		for(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}
		Mentors[mentorIndex].gameObject.SetActive(true);

		yield WaitForSeconds(showTime);

		TipText.text = GetHelpTip();
		TipText.gameObject.SetActive(true);
	}

	function ShowTip(mentorIndex : int, wrong : int)
	{
		StopCoroutine(ShowMentorTip(mentorIndex, wrong));
		StartCoroutine(ShowMentorTip(mentorIndex, wrong));
	}

	function ShowMentorTip(mentorIndex : int, wrong : int)
	{
		yield WaitForSeconds(1.5f);

		for(var m in Mentors)
		{
			m.gameObject.SetActive(false);
		}

		Mentors[mentorIndex].gameObject.SetActive(true);

		yield WaitForSeconds(showTime);

//		TipText.text = GetRandomMentorTip();
//		TipText.text = GetNextTip();
		
		if(wrong == 0)
		{
			TipText.text = "Perfect!";
			repeatTip = 0;
		}
		else if(wrong > 0 && wrong < 4)
		{
			TipText.text =  GetGoodTip();
			repeatTip = 1;
		}
		else if(wrong > 2)
		{
			TipText.text =  GetHelpTip();
			repeatTip = 3;
		}

		TipText.gameObject.SetActive(true);

	}

	private function GetRandomMentorTip(tipsArray : String[])
	{
		return tipsArray[Random.Range(0, tipsArray.Length)];
	}

	private function GetMentorTip(index : int)
	{
		if(index > MentorTips.Length)
		{
			index = 0;
			print("No such index. Return 0");
		}
		return MentorTips[index];
	}

	private function GetNextTip()
	{
		currentTipIndex++;

		if(currentTipIndex >= MentorTips.Length)
		{
			currentTipIndex = 0;
		}
		return MentorTips[currentTipIndex];
	}

	private function GetGoodTip()
	{
		return GetRandomMentorTip(MentorGoodTips)	;
	}

	private function GetHelpTip()
	{
		return GetRandomMentorTip(MentorHelpTips)	;
	}

}