﻿#pragma strict
import UnityEngine.UI;
import UnityEngine.EventSystems;

class MenuSelector extends MonoBehaviour
{
	public var selector : GameObject;
	public var menuItems : GameObject[];
	public var canReadKey : boolean = false;

	var _currentSelected : GameObject;

	var selectorCounter : int = 0;


	function Start () {
		if(menuItems.Length > 0)
		{
			_currentSelected = menuItems[0];
		}
	}

	function Update () {

		if(canReadKey)
		{
			GetInput();
		}
	}

	function GetInput()
	{
		if(Input.GetKeyDown(KeyCode.DownArrow))
			{
				selectorCounter++;
				if(selectorCounter >= menuItems.Length)
				{
					selectorCounter = 0;
				}

				_currentSelected = menuItems[selectorCounter];
				MoveSelector(_currentSelected);
			}
			else if(Input.GetKeyDown(KeyCode.UpArrow))
			{
				selectorCounter--;

				if(selectorCounter < 0)
				{
					selectorCounter = menuItems.Length-1;
				}

				_currentSelected = menuItems[selectorCounter];
				MoveSelector(_currentSelected);
			}
			if(Input.GetKeyDown(KeyCode.LeftArrow))
			{
				selectorCounter++;
				if(selectorCounter >= menuItems.Length)
				{
					selectorCounter = 0;
				}

				_currentSelected = menuItems[selectorCounter];
				MoveSelector(_currentSelected);
			}
			else if(Input.GetKeyDown(KeyCode.RightArrow))
			{
				selectorCounter--;

				if(selectorCounter < 0)
				{
					selectorCounter = menuItems.Length-1;
				}

				_currentSelected = menuItems[selectorCounter];
				MoveSelector(_currentSelected);
			}

			else if(Input.GetKeyDown(KeyCode.Return))
			{
	//			Debug.Log(_currentSelected);
				_currentSelected.GetComponent(Button).onClick.Invoke();
			}
	}

	function MoveSelector(target : GameObject)
	{
		selector.transform.position = target.transform.position;
	}

	public function MoveSelectorOnMouseover(go : GameObject)
	{
		for(var i = 0; i < menuItems.Length; i++)
		{
			var g = menuItems[i];

			if(g.name.Equals(go.name))
			{
				selectorCounter = i;
				_currentSelected = go;
				//MoveSelector(go);
			}
		}

		MoveSelector(_currentSelected);
	}

}
