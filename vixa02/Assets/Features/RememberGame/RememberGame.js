﻿#pragma strict
import System.Collections.Generic;
import UnityEngine.EventSystems;
import UnityEngine.UI;
import UnityEngine.SceneManagement;


class RememberGame extends MonoBehaviour
{
	public var passwordInputField : InputField;

	public var InstructionsPanel : GameObject;
	public var StartGamePanel : GameObject;
	public var WonPanel : GameObject;
	public var GameOverPanel : GameObject;

	public var attempts : int = 5;
	public var PointsToWin : int = 5;

	public var pointValue : int = 1;
	public var pointPenalty : int = 1;
	public var GameTimer : float = 30f;
	public var GameTimerTXT : Text;

	public var passwordsTXT : GameObject[];
	public var Mentors : Mentor;

	private var points : int;
	private var hasWon : boolean = false;
	private var loadedPasswords : String[];
	private var spawnedTexts : List.<GameObject>;

	private var timeLeft : float;
	private var hasGameStarted : boolean = false;
	private var wrongGuesses : int = 0;

	public var guessedPasswordsCounter : int;
	private var revealedPasswords = new Array(String);

	public var stack = new Array(String);

	function Start () {

		timeLeft = GameTimer;

		spawnedTexts = new List.<GameObject>();

		passwordInputField.text = "";
		HidePasswords();
		LoadPasswords();

		//print("------------------");
		//stack.pop();
		//print(stack);
		//print(stack.length);
		stack.pop();
		//print(stack);

		//print(stack.length);
		Mentors = GetComponentInChildren(typeof(Mentor), true);
	}

	function Update () {

		if(!hasGameStarted)
			return;
		
		timeLeft -= Time.deltaTime;
		GameTimerTXT.text = String.Format ("{0:00}", timeLeft);
		if ( timeLeft < -0.4f )
		{
				GameOver();
		}
	}

	public function StartGame()
	{
		hasGameStarted = true;
		InstructionsPanel.SetActive(false);
		StartGamePanel.SetActive(true);
		passwordInputField.ActivateInputField();
	}

	public function CheckInput()
	{
		var input = passwordInputField.text;
		var spawnText = "";
		if(input.Equals(""))
		{
			print("nothing");

		}
		else
		{	
			if(input in revealedPasswords)
			{
				spawnText = "Already guessed!";
			}
			else if(input in loadedPasswords)
			{
				RevealPassword(input);
				spawnText = "Great";

				if(guessedPasswordsCounter == 0)
				{
					GameWon();
				}
			}
			else
			{
				spawnText = "Not in the list";
				wrongGuesses++;
			}
			StartCoroutine(SpawnText( spawnText ));
		}

		passwordInputField.text = "";
		passwordInputField.ActivateInputField();
	}

	private function RevealPassword(password : String)
	{
		var index = -1;

		for(var i = 0; i < loadedPasswords.length; i++)
		{
			if(password.Equals(loadedPasswords[i]))
			{
				index = i;
				passwordsTXT[index].GetComponent(Text).text = password;
				passwordsTXT[index].SetActive(true);

				if(!(password in revealedPasswords))
				{
					revealedPasswords.push(password);
					guessedPasswordsCounter--;
				}

				return;
			}
		}

	}

	private function HidePasswords()
	{
		for(var i = 0; i < passwordsTXT.length; i++)
		{
			passwordsTXT[i].SetActive(false);
		}
	}

	private function LoadPasswords()
	{
		loadedPasswords = PlayerPrefsX.GetStringArray("passwords");

		guessedPasswordsCounter = loadedPasswords.length;
		print("loadedPasswords length = " + loadedPasswords.length);
		for(var i = 0; i < loadedPasswords.length; i++)
		{
			print(loadedPasswords[i]);
		}

	}

	public function AddPoint(text : String)
	{
		points += pointValue;
		//UIShowPassScore(points.ToString(), text);

		if(points == PointsToWin)
		{
			hasWon = true;
		}

	}

	public function RemovePoint(text : String)
	{
		var tempPoint : int = points - pointPenalty;
		if(tempPoint < 0)
		{
			points = 0;
		}
		else
		{
			points = tempPoint;
		}

		//UIShowPassScore(points.ToString(), text);
	}

	private function SpawnText( text : String)
	{
		var go : GameObject = ObjectPool.instance.GetObjectForType("SpawnText", false);
		go.transform.SetParent(transform.root, false);
		go.GetComponentInChildren(Text).text = text;
		spawnedTexts.Add(go);
		yield WaitForSeconds(1);

		ObjectPool.instance.PoolObject(go);
		spawnedTexts.Remove(go);
	}

	private function ClearSpawnedTexts()
	{
		for(var st in spawnedTexts)
		{
			ObjectPool.instance.PoolObject(st);
		}
		spawnedTexts.Clear();
	}

	public function OnRestart()
	{
		Time.timeScale = 1f;
		SceneManager.LoadScene("03.RememberPassword");
	}

	public function OnMainMenu()
	{
		SceneManager.LoadScene("00.MainMenu");
	}

	public function GameWon()
	{
		passwordInputField.interactable = false;
		passwordInputField.DeactivateInputField();
		WonPanel.SetActive(true);
		Mentors.gameState = Mentors.GameState.RememberGame;
		Mentors.ShowTip(Random.Range(0, Mentors.Mentors.Length), wrongGuesses);
	}

	public function GameOver()
	{
		passwordInputField.interactable = false;
		passwordInputField.DeactivateInputField();
		GameTimerTXT.text = String.Format ("{0:00}", 0);
		GameOverPanel.SetActive(true);
	}

}