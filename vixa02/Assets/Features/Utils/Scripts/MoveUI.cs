﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MoveUI : MonoBehaviour {

	public GameObject image;
	public GameObject StartPoint;
	public GameObject EndPoint;
	public float timeToReachTarget;

	// Use this for initialization
	void Start () {
		//StartCoroutine(MoveToPosition(transform, EndPoint.transform.position, timeToReachTarget));
	}
	
	// Update is called once per frame
	void Update () {
		//t += Time.deltaTime/timeToReachTarget;
		//transform.position = Vector3.Lerp(startPosition, target, t);

		if(Input.GetKeyDown(KeyCode.Space))
		{
			transform.position = StartPoint.transform.position;
			StartCoroutine(MoveToPosition(transform, EndPoint.transform.position, timeToReachTarget));
		}
	}



	public IEnumerator MoveToPosition(Transform transform, Vector3 position, float timeToMove)
	{
		var currentPos = transform.GetComponent<RectTransform>().position ; //transform.position;
		var t = 0f;
		while(t < 1)
		{
			t += Time.deltaTime / timeToMove;
			Vector3 newPos = Vector3.Lerp(currentPos, position, t);
			transform.GetComponent<RectTransform>().position = newPos;

			//GetComponent<RectTransform> ().anchoredPosition = Vector3.Lerp(currentPos, position, t);
			//GetComponent<RectTransform> ().offsetMax = Vector2.Lerp (oldPositionMax, newPositionMax, percentage);
			//GetComponent<RectTransform> ().offsetMin = Vector2.Lerp (oldPositionMin, newPositionMin, percentage);
			yield return null;
		}
	}
}
